#import "VTNodeConnectionHelper.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <Node_iOS/Node_iOS.h>

static NSString * const VTNodeConnectionHelperSavedDeviceUUIDKey = @"VTNodeConnectionHelperSavedDeviceUUIDKey";
static NSString * const VTNodeConnectionHelperSavedDeviceNameKey = @"VTNodeConnectionHelperSavedDeviceNameKey";

@interface VTNodeConnectionHelper () <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (strong, nonatomic) CBCentralManager *cb_central_manager;
@property (strong, nonatomic) NSMutableArray *peripherals;
@property (nonatomic) BOOL shouldAutoScan;
@property (strong, nonatomic) NSTimer *stopScanTimer;
@property (strong, nonatomic) NSTimer *connectionFailedTimer;
@end

@implementation VTNodeConnectionHelper

#pragma mark - Initialization
- (id)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(id)initWithDelegate:(NSObject<VTNodeConnectionHelperDelegate> *)delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        [self commonInit];
    }
    return self;
}

+ (VTNodeConnectionHelper *) connectionHelperwithDelegate:(NSObject<VTNodeConnectionHelperDelegate> *)delegate {
    VTNodeConnectionHelper *helper = [[VTNodeConnectionHelper alloc] initWithDelegate:delegate];
    return helper;
}

-(void) commonInit
{
    self.isScanning = NO;
    self.shouldAutoScan = NO;
    NSDictionary *central_options = @{CBCentralManagerOptionShowPowerAlertKey : @YES};
    self.cb_central_manager = [[CBCentralManager alloc] initWithDelegate:self queue:Nil options:central_options];
    self.peripherals = [NSMutableArray array];
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if(central.state == CBCentralManagerStatePoweredOn) {
        if(self.shouldAutoScan == YES) {
            [self startScanAndRetrievalOfPeripherals];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    if([self.peripherals containsObject:peripheral] == NO) {
        [self.peripherals addObject:peripheral];
        [self sortPeripheralsList];
        [self checkIfThisDeviceIsDefault:peripheral];
        [self.delegate nodeConnectionHelperDidUpdateNodeDeviceList];
    }
}

#pragma mark - Peripheral Discovery
- (void) startScanAndRetrievalOfPeripherals {
    if(self.cb_central_manager.state != CBCentralManagerStatePoweredOn) {
        self.shouldAutoScan = YES;
        return;
    }
    
    if(self.isScanning) {
        [self.cb_central_manager stopScan];
        self.isScanning = NO;
    }
    
    if(self.isScanning == NO) {
        self.peripherals = [NSMutableArray array];
        self.isScanning = YES;
        
        NSDictionary *options = @{CBCentralManagerScanOptionAllowDuplicatesKey : @NO,
                                  CBCentralManagerScanOptionSolicitedServiceUUIDsKey : [VTNodeDevice nodeServiceUUIDs]};
        [self.cb_central_manager scanForPeripheralsWithServices:[VTNodeDevice nodeServiceUUIDs] options:options];
        
        /* Stop the scan after some time */
        if(_stopScanTimer) {
            [_stopScanTimer invalidate];
            _stopScanTimer = nil;
        }
        _stopScanTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(stopScanningAndInformDelegate) userInfo:nil repeats:NO];
        
        /* Also retrieve already connected peripherals */
        [self fetchConnectedPeripherals];
    }
}

- (void) stopScanningAndInformDelegate
{
    [self.cb_central_manager stopScan];
    if([self.delegate respondsToSelector:@selector(nodeConnectionHelperDidFinishScanning)]) {
        [self.delegate nodeConnectionHelperDidFinishScanning];
    }
    self.isScanning = NO;
}

-(void)fetchConnectedPeripherals {
    NSArray *connected_peripherals = [self.cb_central_manager retrieveConnectedPeripheralsWithServices:[VTNodeDevice nodeServiceUUIDs]];
    
    for(CBPeripheral *peripheral in connected_peripherals) {
        if(![self.peripherals containsObject:peripheral]) {
            [self.peripherals addObject:peripheral];
            [self sortPeripheralsList];
            [self.delegate nodeConnectionHelperDidUpdateNodeDeviceList];
            [self checkIfThisDeviceIsDefault:peripheral];

        }
    }
}

#pragma mark - Device List Access
- (void) sortPeripheralsList {
    [self.peripherals sortUsingComparator:^NSComparisonResult(CBPeripheral *p1, CBPeripheral *p2) {
        return [p1.name compare:p2.name];
    }];
}

- (NSArray *)allNodeDevices {
    return [NSArray arrayWithArray:self.peripherals];
}

#pragma mark - Connection
- (void)connectDevice: (CBPeripheral *)device forUseInBackground: (BOOL) backgroundEnabled {
    NSLog(@"connectDevice");
    NSNumber *bg_enabled = [NSNumber numberWithBool:backgroundEnabled];
    NSDictionary *options = @{
                              CBConnectPeripheralOptionNotifyOnConnectionKey : bg_enabled,
                              CBConnectPeripheralOptionNotifyOnDisconnectionKey : bg_enabled,
                              CBConnectPeripheralOptionNotifyOnNotificationKey : bg_enabled
                              };
    
    [self.cb_central_manager stopScan];

    [self.cb_central_manager connectPeripheral:device options:options];
    
    if(self.connectionFailedTimer) {
        [_connectionFailedTimer invalidate];
        _connectionFailedTimer = nil;
    }
    
    _connectionFailedTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(connectionFailed) userInfo:device repeats:NO];
}

- (void) connectionFailed
{
    NSLog(@"connectionFailed");
    [_connectionFailedTimer invalidate];
    _connectionFailedTimer = nil;
    [self.delegate nodeConnectionHelperDidFailToConnectPeripheral:_connectionFailedTimer.userInfo error:nil];
}

- (void)disconnectDevice: (CBPeripheral *)device {
    NSLog(@"disconnectDevice");
    [self.cb_central_manager cancelPeripheralConnection:device];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"didConnectPeripheral");
    if([self.delegate respondsToSelector:@selector(nodeConnectionHelperDidConnectToPeripheral:)]) {
        if(_connectionFailedTimer) {
            [_connectionFailedTimer invalidate];
            _connectionFailedTimer = nil;
        }
        [self.delegate nodeConnectionHelperDidConnectToPeripheral:peripheral];
    }
}

- ( void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"didDisconnectPeripheral");
    if([self.delegate respondsToSelector:@selector(nodeConnectionHelperDidDisconnectFromPeripheral:error:)]) {
        [self.delegate nodeConnectionHelperDidDisconnectFromPeripheral:peripheral error:error];
    }
}

-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    if([self.delegate respondsToSelector:@selector(nodeConnectionHelperDidFailToConnectPeripheral:error:)])
        [self.delegate nodeConnectionHelperDidFailToConnectPeripheral:peripheral error:error];
}

#pragma mark - NODE Saving / Autoconnect
- (NSString *) defaultDeviceUUID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:VTNodeConnectionHelperSavedDeviceUUIDKey];
}

- (NSString *) defaultDeviceName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:VTNodeConnectionHelperSavedDeviceNameKey];
}

- (BOOL) hasDefaultDeviceSaved
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uuid = [defaults stringForKey:VTNodeConnectionHelperSavedDeviceUUIDKey];
    return (uuid == nil) ? false : true;
}

- (void) saveDeviceAsDefault: (CBPeripheral *)peripheral
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:peripheral.identifier.UUIDString forKey:VTNodeConnectionHelperSavedDeviceUUIDKey];
    [defaults setObject:peripheral.name forKey:VTNodeConnectionHelperSavedDeviceNameKey];
    [defaults synchronize];
}

- (void) forgetDefaultDevice {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:VTNodeConnectionHelperSavedDeviceUUIDKey];
    [defaults setObject:nil forKey:VTNodeConnectionHelperSavedDeviceNameKey];
    [defaults synchronize];
}

- (void) checkIfThisDeviceIsDefault: (CBPeripheral *)peripheral
{
    NSString *savedDeviceUUID = self.defaultDeviceUUID;
    
    if(savedDeviceUUID != nil) {
        if([peripheral.identifier.UUIDString isEqualToString:savedDeviceUUID]) {
            NSLog(@"Saved device found.");
            [self.delegate nodeConnectionHelperDidDiscoverDefaultPeripheral:peripheral];
        }
    }
}

@end
