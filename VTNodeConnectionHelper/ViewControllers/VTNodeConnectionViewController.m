//
//  NodeConnectionViewController.m
//  TKHealthCheck
//
//  Created by Wade Gasior on 10/15/14.
//  Copyright (c) 2014 Variable. All rights reserved.
//

#import "VTNodeConnectionViewController.h"
#import "VTNodeConnectionHelper.h"
#import "MBProgressHUD.h"
#import "VTNodeSingleton.h"
#import "VTNodeConnectedViewController.h"
#import <Node_iOS/Node.h>

static NSString * const CELL_IDENTIFIER = @"cell";
static NSString * const SCAN_TEXT = @"Searching for NODE+ Devices";
static NSString * const VTNodeConnectionViewControllerHintText = @"Choose a NODE+ below to connect";
static const int VTNodeConnectionViewControllerCellHeight = 65;

@interface VTNodeConnectionViewController () <VTNodeConnectionHelperDelegate, UITableViewDataSource, UITableViewDelegate, NodeDeviceDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) UILabel *connectHintLabel;
@property (strong, nonatomic) MBProgressHUD *hud;
@property (strong, nonatomic) NSTimer *connectionFailedTimer;
@property (strong, nonatomic) CBPeripheral *connectingPeripheral;
@property (strong, nonatomic) VTNodeDevice *tNodeDevice;
@end

@implementation VTNodeConnectionViewController

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    _tableView.hidden = NO;
    _connectHintLabel.hidden = NO;
    [VTNodeSingleton getInstance].nodeConnectionHelper.delegate = self;
    [[VTNodeSingleton getInstance].nodeConnectionHelper startScanAndRetrievalOfPeripherals];
    
    if([VTNodeSingleton getInstance].nodeConnectionHelper.hasDefaultDeviceSaved) {
        NSLog(@"Default device is saved - attempting connection to %@", [VTNodeSingleton getInstance].nodeConnectionHelper.defaultDeviceName);
        _tableView.hidden = YES;
        _connectHintLabel.hidden = YES;
        [self hideHud];
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *savedDeviceName = [VTNodeSingleton getInstance].nodeConnectionHelper.defaultDeviceName;
        self.hud.labelText = [NSString stringWithFormat: @"Searching for %@",  savedDeviceName];
    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if(_connectionFailedTimer) {
        [_connectionFailedTimer invalidate];
        _connectionFailedTimer = nil;
    }
}

- (void) loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    [self configureTableView];
}

- (void) hideHud
{
    if(self.hud != nil) {
        [self.hud hide:NO];
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void) configureTableView
{
    _connectHintLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 80)];
    _connectHintLabel.text = VTNodeConnectionViewControllerHintText;
    _connectHintLabel.textAlignment = NSTextAlignmentCenter;
    _connectHintLabel.backgroundColor = [UIColor darkGrayColor];
    _connectHintLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:_connectHintLabel];
    
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CELL_IDENTIFIER];
    [self.view addSubview:self.tableView];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    [_refreshControl addTarget:self
                            action:@selector(refreshDeviceList:)
                  forControlEvents:UIControlEventValueChanged];
    _refreshControl.attributedTitle = [[NSMutableAttributedString alloc] initWithString:SCAN_TEXT];
    
    [self.tableView addSubview:_refreshControl];
    [_refreshControl beginRefreshing];
}

- (void) viewWillLayoutSubviews
{
    _connectHintLabel.frame = CGRectMake(0, 50, self.view.bounds.size.width, 50);
    _tableView.frame = CGRectMake(0, 100, self.view.bounds.size.width, self.view.bounds.size.height - 50);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [VTNodeSingleton getInstance].nodeConnectionHelper.allNodeDevices.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    CBPeripheral *cb = [[VTNodeSingleton getInstance].nodeConnectionHelper.allNodeDevices objectAtIndex:indexPath.row];
    if(cb.name == nil || [cb.name isEqualToString:@""]) {
        cell.textLabel.text = cb.identifier.UUIDString;
    }
    else {
        cell.textLabel.text = cb.name;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return VTNodeConnectionViewControllerCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *cb = [[VTNodeSingleton getInstance].nodeConnectionHelper.allNodeDevices objectAtIndex:indexPath.row];
    [self connectToPeripheral:cb];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    act.center = CGPointMake(cell.bounds.size.width-50, cell.bounds.size.height/2);
    [act startAnimating];
    [cell addSubview:act];
}

- (void) connectToPeripheral: (CBPeripheral *)peripheral
{
    if(peripheral == nil) {
        return;
    }
    self.connectingPeripheral = peripheral;
    [self hideHud];
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _tableView.hidden = YES;
    _connectHintLabel.hidden = YES;
    self.hud.labelText = [NSString stringWithFormat:@"Connecting to %@", peripheral.name];
    
    if(self.connectionFailedTimer) {
        [_connectionFailedTimer invalidate];
        _connectionFailedTimer = nil;
    }
    
    _connectionFailedTimer = [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(checkIfConnectionFailed) userInfo:peripheral repeats:NO];
    
    [[VTNodeSingleton getInstance].nodeConnectionHelper connectDevice:peripheral forUseInBackground:YES];
}

#pragma mark - NodeConnectionHelperDelegate
- (void) nodeConnectionHelperDidUpdateNodeDeviceList {
    NSLog(@"nodeConnectionHelperDidUpdateNodeDeviceList");
    [self.tableView reloadData];
}

- (void) nodeConnectionHelperDidFinishScanning {
    NSLog(@"nodeConnectionHelperDidFinishScanning");
    [self.refreshControl endRefreshing];
    if( [VTNodeSingleton getInstance].nodeConnectionHelper.hasDefaultDeviceSaved ) {
        if([VTNodeSingleton getInstance].nodeDevice == nil) {
            [self showDefaultDeviceTimeout];
        }
    }
}

- (void) showDefaultDeviceTimeout
{
    if(self.connectingPeripheral) {
        return;
    }
    
    [self hideHud];
    
    NSString *defaultDeviceName = [VTNodeSingleton getInstance].nodeConnectionHelper.defaultDeviceName;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat: @"%@ Not Found", defaultDeviceName]
                                                                             message:[NSString stringWithFormat: @"%@ was not found. Please ensure the device is powered on and within range.", defaultDeviceName]
                                                                      preferredStyle:UIAlertControllerStyleAlert];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self hideHud];
        [self viewWillAppear:NO];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Choose Other Device" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [[VTNodeSingleton getInstance].nodeConnectionHelper forgetDefaultDevice];
        [self hideHud];
        [self viewWillAppear:NO];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) nodeConnectionHelperDidConnectToPeripheral:(CBPeripheral *)peripheral {
    self.tNodeDevice = [[VTNodeDevice alloc] initWithDelegate:self withDevice:peripheral];
}

- (void) nodeConnectionHelperDidDiscoverDefaultPeripheral:(CBPeripheral *)peripheral
{
    [self connectToPeripheral:peripheral];
}

- (void) checkIfConnectionFailed
{
    NSLog(@"checkIfConnectionFailed");
    if([VTNodeSingleton getInstance].nodeDevice == nil) {
        [self showConnectFailedDialogForPeripheral:_connectionFailedTimer.userInfo];
        self.connectingPeripheral = nil;
    }
}

- (void) nodeConnectionHelperDidFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    self.connectingPeripheral = nil;
    [self showConnectFailedDialogForPeripheral:peripheral];
}

- (void) showConnectFailedDialogForPeripheral: (CBPeripheral *)peripheral
{
    if(peripheral) {
        if(self.presentedViewController == nil) {
            [self hideHud];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Failed to connect"
                                                                                     message:@"Connection to NODE+ failed."
                                                                              preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self connectToPeripheral:peripheral];
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Choose Other Device" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [[VTNodeSingleton getInstance].nodeConnectionHelper forgetDefaultDevice];
                [self hideHud];
                [self viewWillAppear:NO];
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }

    }
}

#pragma mark - Refreshing
-(void)refreshDeviceList:(UIRefreshControl *)refresh {
    [[VTNodeSingleton getInstance].nodeConnectionHelper startScanAndRetrievalOfPeripherals];
}

-(void)nodeDeviceFailedToInit:(VTNodeDevice *)device
{
    [self showConnectFailedDialogForPeripheral:device.peripheral];
}

#pragma mark - Node Device Delegate
- (void) nodeDeviceIsReadyForCommunication:(VTNodeDevice *)device
{
    [VTNodeSingleton getInstance].nodeDevice = device;
    [self hideHud];
    NSLog(@"nodeDeviceIsReadyForCommunication");
    [[VTNodeSingleton getInstance].nodeConnectionHelper saveDeviceAsDefault:device.peripheral];
    [VTNodeSingleton getInstance].nodeConnectionHelper.delegate = [VTNodeSingleton getInstance];
    
    [VTNodeSingleton getInstance].nodeDevice = device;
    
    //ColorMuse and Therma2 require a ble connection confirmation
    if (device.isColorMuse || device.isTherma2){
        [device sendConnectionConfirmation];
    }
    
    VTNodeConnectedViewController *vc = [[VTNodeConnectedViewController alloc] init];
    [self presentViewController:vc animated:YES completion:nil];
}



@end
