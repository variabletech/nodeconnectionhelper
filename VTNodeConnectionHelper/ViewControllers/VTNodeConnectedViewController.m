//
//  VTNodeConnectedViewController.m
//  TKHealthCheck
//
//  Created by Wade Gasior on 10/16/14.
//  Copyright (c) 2014 Variable. All rights reserved.
//

#import "VTNodeConnectedViewController.h"
#import "VTNodeConnectionHelper.h"
#import "VTNodeSingleton.h"

static const int VTNodeConnectedViewControllerStandardLabelHeight = 30;
static const int VTNodeConnectedViewControllerDeviceNameLabelHeight = 50;
static const float VTNodeConnectedViewControllerConnectedToLabelFontSize = 20.0f;
static const float VTNodeConnectedViewControllerDeviceNameLabelFontSize = 32.0f;
static const float VTNodeConnectedViewDisconnectButtonHeight = 75.0f;
static const float VTNodeConnectedViewVerticalSpacingToDetailsSection = 75.0f;

@interface VTNodeConnectedViewController ()
@property (strong, nonatomic) UILabel *connectedToLabel;
@property (strong, nonatomic) UILabel *deviceNameLabel;
@property (strong, nonatomic) UIButton *disconectButton;
@property (strong, nonatomic) UILabel *batteryLevelLabel;
@property (strong, nonatomic) UILabel *firmwareVersionLabel;
@property (strong, nonatomic) UILabel *serialNumberLabel;
@end

@implementation VTNodeConnectedViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    [VTNodeSingleton getInstance].nodeConnectionHelper.delegate = self;
}

- (void) viewWillDisappear:(BOOL)animated
{
    //Reassign the NODE delegate back to the singleton
    [VTNodeSingleton getInstance].nodeConnectionHelper.delegate = [VTNodeSingleton getInstance];
}

- (void) viewDidLoad
{
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    _connectedToLabel = [[UILabel alloc] initWithFrame:self.view.frame];
    _connectedToLabel.text = @"Connected To";
    _connectedToLabel.font = [UIFont boldSystemFontOfSize:VTNodeConnectedViewControllerConnectedToLabelFontSize];
    _connectedToLabel.textColor = [UIColor darkGrayColor];
    _connectedToLabel.textAlignment = NSTextAlignmentCenter;
    
    _deviceNameLabel = [[UILabel alloc] initWithFrame:self.view.frame];
    _deviceNameLabel.text = [VTNodeSingleton getInstance].nodeDevice.peripheral.name;
    _deviceNameLabel.font = [UIFont boldSystemFontOfSize:VTNodeConnectedViewControllerDeviceNameLabelFontSize];
    _deviceNameLabel.textColor = [UIColor blackColor];
    _deviceNameLabel.textAlignment = NSTextAlignmentCenter;
    
    
    _disconectButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _disconectButton.frame = self.view.frame;
    [_disconectButton setTitle:@"Disconnect" forState:UIControlStateNormal];
    [_disconectButton addTarget:self action:@selector(disconnectFromDevice) forControlEvents:UIControlEventTouchUpInside];
    
    _batteryLevelLabel = [[UILabel alloc] initWithFrame:self.view.frame];
    _batteryLevelLabel.text = [NSString stringWithFormat: @"%0.2f V", [VTNodeSingleton getInstance].nodeDevice.batteryLevel];
    _batteryLevelLabel.textColor = [UIColor darkGrayColor];
    _batteryLevelLabel.textAlignment = NSTextAlignmentCenter;
    
    _firmwareVersionLabel = [[UILabel alloc] initWithFrame:self.view.frame];
    _firmwareVersionLabel.text = [NSString stringWithFormat: @"%u.%u", [VTNodeSingleton getInstance].nodeDevice.firmwareMajorRev, [VTNodeSingleton getInstance].nodeDevice.firmwareMinorRev];
    _firmwareVersionLabel.textColor = [UIColor darkGrayColor];
    _firmwareVersionLabel.textAlignment = NSTextAlignmentCenter;
    
    _serialNumberLabel = [[UILabel alloc] initWithFrame:self.view.frame];
    _serialNumberLabel.text = [VTNodeSingleton getInstance].nodeDevice.serialNumber;
    _serialNumberLabel.textColor = [UIColor darkGrayColor];
    _serialNumberLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:_connectedToLabel];
    [self.view addSubview:_deviceNameLabel];
    [self.view addSubview:_disconectButton];
    [self.view addSubview:_batteryLevelLabel];
    [self.view addSubview:_firmwareVersionLabel];
    [self.view addSubview:_serialNumberLabel];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    float W = self.view.frame.size.width;
    float H = self.view.frame.size.height;
    
    float oneQuarterDown = 0.25 * H;
    float currentY = oneQuarterDown;
    
    _connectedToLabel.frame = CGRectMake(0, currentY, W, VTNodeConnectedViewControllerStandardLabelHeight);
    currentY += VTNodeConnectedViewControllerStandardLabelHeight;
    _deviceNameLabel.frame = CGRectMake(0, currentY, W, VTNodeConnectedViewControllerDeviceNameLabelHeight);
    currentY += VTNodeConnectedViewControllerDeviceNameLabelHeight + VTNodeConnectedViewVerticalSpacingToDetailsSection;
    
    _serialNumberLabel.frame = CGRectMake(0, currentY, W, VTNodeConnectedViewControllerStandardLabelHeight);
    currentY += VTNodeConnectedViewControllerStandardLabelHeight;
    _batteryLevelLabel.frame = CGRectMake(0, currentY, W, VTNodeConnectedViewControllerStandardLabelHeight);
    currentY += VTNodeConnectedViewControllerStandardLabelHeight;
    _firmwareVersionLabel.frame = CGRectMake(0, currentY, W, VTNodeConnectedViewControllerStandardLabelHeight);
    
    
    _disconectButton.frame = CGRectMake(0, H-VTNodeConnectedViewDisconnectButtonHeight, W, VTNodeConnectedViewDisconnectButtonHeight);
}

- (void) disconnectFromDevice
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Remember device?"
                                                                             message:[NSString stringWithFormat: @"Would you like to keep %@ as your default device?", [VTNodeSingleton getInstance].nodeDevice.peripheral.name]
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self disconnectAndGoBack:NO];

    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Forget Device" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self disconnectAndGoBack:YES];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) disconnectAndGoBack: (BOOL) forgetDevice
{
    if(forgetDevice) {
        [[VTNodeSingleton getInstance].nodeConnectionHelper forgetDefaultDevice];
    }
    [[VTNodeSingleton getInstance].nodeConnectionHelper disconnectDevice:[VTNodeSingleton getInstance].nodeDevice.peripheral];
    [VTNodeSingleton getInstance].nodeDevice = nil;
     
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) nodeConnectionHelperDidDisconnectFromPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    [VTNodeSingleton getInstance].nodeDevice = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) nodeConnectionHelperDidUpdateNodeDeviceList
{
    //Do nothing
}

@end
