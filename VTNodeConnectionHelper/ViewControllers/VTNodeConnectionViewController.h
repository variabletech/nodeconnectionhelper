//
//  NodeConnectionViewController.h
//  TKHealthCheck
//
//  Created by Wade Gasior on 10/15/14.
//  Copyright (c) 2014 Variable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Node_iOS/Node_iOS.h>

@interface VTNodeConnectionViewController : UIViewController

@end
