//
//  VTNodeConnectedViewController.h
//  TKHealthCheck
//
//  Created by Wade Gasior on 10/16/14.
//  Copyright (c) 2014 Variable. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeConnectionHelper.h"

@interface VTNodeConnectedViewController : UIViewController <VTNodeConnectionHelperDelegate>

@end
