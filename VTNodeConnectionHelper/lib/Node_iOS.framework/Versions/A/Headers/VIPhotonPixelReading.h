//
//  VIPhotonPixelReading.h
//  Node_iOS
//
//  Created by Corey Mann on 1/14/16.
//  Copyright © 2016 Variable, Inc. All rights reserved.
//


#ifndef VIPhotonPixelReading_h
#define VIPhotonPixelReading_h

@class VISpectralColor;

@interface VIPhotonPixelReading : NSObject


- (id) initWithSerial: (NSString*) serial withPixels: (NSData*) pixels;

- (VISpectralColor*) toSpectralColor;

@property (readonly, strong)  NSString* serial;
@property (readonly, strong)  NSData*  pixels;

@end
#endif /* VIPhotonPixelReading_h */
