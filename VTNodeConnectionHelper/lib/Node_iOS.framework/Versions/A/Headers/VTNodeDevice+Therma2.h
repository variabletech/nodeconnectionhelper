//
//  VTNodeDevice+Therma2.h
//  Node_iOS
//
//  Created by Andrew T on 11/18/16.
//  Copyright © 2016 Variable, Inc. All rights reserved.
//

#import "VTNodeDevice.h"

@interface VTNodeDevice (Therma2)


/** Request single thermocouple probe reading - (response via nodeDeviceDidTransmitThermocoupleReading:)
 */
-(void)t2RequestProbeReading;


/** Request single IR reading - (response via nodeDeviceDidUpdateIRThermaReading:withReading:)
 */
-(void)t2RequestIRReading;


/** Request T2 temperature scale (C/F) used by Therma2 OLED Display - (response via t2DidTransmitTemperatureScale:scale:)
 */
-(void)t2RequestTemperatureScale;


/** Request T2 to provide the T2 temperature shown Therma2 OLED Display before BLE was connected
    Response will come from either T2 IR or Probe temperature reading delegate methods
 */
-(void)t2RequestUnconnectedTemperature;



/** Set T2 temperature scale (C/F) to be used by Therma2 OLED Display
 @param scale - Celcius / Fahrenheit
 */
-(void)t2SetTemperatureScale:(VTNodeTemperatureScale)scale;



/** Enable or disable Therma2 IR streaming using a specified period and duration 
    Note: if looking for Probe streaming - this is done using Node+Therma streaming method - setStreamModeThermocouple:
 
 @param enabled set to true to start, false to stop streaming
 @param emiss CURRENTLY UNSUPPORTED - hardset to 0.95
 @param p The period between readings in units of SECONDS
 @param life The number of readings to collect (0 for infinite)
 
 */
-(void) setStreamModeIRT2:(bool)enabled emissivity:(float)emiss withPeriod:(uint8_t)p withLifetime:(uint8_t)life;


@end
