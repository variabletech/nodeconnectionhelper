#import <Foundation/Foundation.h>
#import "VTXYZColor.h"
#import "VTLCHColor.h"
#import "VTColorUtils.h"
#import "VTLchColor.h"

typedef enum {VTLAB_DE94_GRAPHIC, VTLAB_DE94_TEXTILE} vtlabDE94Type;
typedef enum {VTLAB_DECMC_2_1,VTLAB_DECMC_1_1} vtlabDEcmcType;



@interface VTLabColor : NSObject
@property (nonatomic, readonly) double L;
@property (nonatomic, readonly) double A;
@property (nonatomic, readonly) double B;
@property (readonly, strong) VTLchColor *lchColor;
@property (nonatomic, readonly) VTXYZColor *XYZColor;
@property (nonatomic, readonly) VTColorUtilsIlluminant illuminant;
@property (nonatomic, readonly) VTColorUtilsObserver stdObserver;
@property (readonly, nonatomic) NSDictionary *json;
- (NSString *) displayString;
- (VTLabColor *) toLabWithIlluminant: (VTColorUtilsIlluminant) ill;


+ (instancetype) labColorWithL: (double) l a: (double) a b: (double) b usingIll: (VTColorUtilsIlluminant) illuminant;
+ (instancetype) labColorWithL: (double) l a: (double) a b: (double) b usingIll: (VTColorUtilsIlluminant) illuminant usingObserver:(VTColorUtilsObserver)observer;


-(double)deltaE00:(VTLabColor*)other;
-(double)deltaEcmc:(VTLabColor*)other dEcmcType:(vtlabDEcmcType)dEcmcType;
-(double)deltaE94:(VTLabColor*)other dE94Type:(vtlabDE94Type)dE94Type;

@end
