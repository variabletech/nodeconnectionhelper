//
//  VTNodeDevice+Chroma.h
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//
#include "VIChromaSession.h"
#include "VIChromaMatchLibrary.h"
#include "VTColorUtils.h"
#include "VIColorReading.h"


@interface VTNodeDevice (Chroma)


/** Initialize the attached Chroma device
 @param success Callback block that will reflect Chroma ready
 @param fail Callback block that indicates something went wrong
 */
-(void)chromaInit:(void(^)(BOOL chromaReady))success onFail:(void(^)(NSError *error))fail;

/** Initialize a Chroma device via serial (Chroma Device doesn't necessarily have to be attached)
 @param serial Serial# of the Chroma Module to register.
 @param success Callback block that will reflect Chroma ready
 @param fail Callback block that indicates something went wrong
 */
+(void)chromaInitWithSerial:(NSString*)serial success:(void(^)(BOOL chromaReady))success fail:(void(^)(NSError *error))fail;

/** Request a Chroma reading using 2˚ or 10˚ math
 @param observer Standard Observer functions (options are either 1964 10˚ observer or 1931 2˚ observer (2˚ is the default and most commonly used)
 @param color Callback block that will provide the color reading as a VIColorReading object
 @param temp Callback block that will provide the temperature at the time of reading (can be useful to prompt re-calibration if temp changes alot)
 @param fail Callback block that indicates something went wrong
 */
-(void)requestChromaReadingForObserver:(VTColorUtilsObserver)observer color:(void(^)(VIColorReading* reading, NSString *analyticsUUID))color temp:(void(^)(float))tempCB fail:(void(^)(NSError*))fail;

/** Request Chroma  1.1+ to perform whitepoint calibration using end cap
 @param success Response block that will pass a BOOL indicating if WP succeeded or failed. WP Cal will not be applied until success.
 */
-(void)requestChromaWhitePointCal:(void(^)(BOOL)) success;

/** Clear Chroma  1.1+ whitepoint calibration - use default calibration
 */
-(void)clearChromaWhitePointCal;


/** Request Chroma raw sense values array (currently only supported for ChromaNIR)
 @param senseCallback Response block that will pass NSArray of sense numbers upon completion.
 @param fail Callback block that indicates something went wrong
 */
-(void)requestChromaSenseValuesForPort: (VTModuleLocation)port onSuccess:(void(^)(NSArray* sense)) senseCallback fail:(void(^)(NSError*))fail;

/**
    Request a Chroma match reading using all Color Libraries associated with this account
    @param onResult A completion block that will asynchronously provide VIChromaMatchResults upon Chroma scan completion.
 */

-(void)requestChromaMatchReadingUsingLibraries: (NSArray *)libs
                                      onResult: (void (^)(NSArray *results, VIColorReading *reading, NSString *eventUUID))onResult;

-(void)requestChromaMatchReadingUsingLibraries: (NSArray *)libs
                             withAnalyticsData: (NSDictionary *)analyticsData
                                      onResult: (void (^)(NSArray *results, VIColorReading *reading, NSString *eventUUID))onResult;

@end