#import <Foundation/Foundation.h>
#import "VTColorUtils.h"
#import "VTRGBColor.h"
@class VTLabColor;
@class VTLuvColor;
@class VTLchColor;

@interface VTXYZColor : NSObject
//X,Y,Z are 100 base scaled
@property (nonatomic, readonly) double X;
@property (nonatomic, readonly) double Y;
@property (nonatomic, readonly) double Z;
@property (nonatomic, readonly) VTColorUtilsIlluminant illuminant;
@property (nonatomic, readonly) VTColorUtilsObserver stdObserver;

//Constructors expect 100 scaled XYZ values
+ (instancetype) XYZColorWithX: (double) x Y: (double) y Z: (double)z usingIll: (VTColorUtilsIlluminant) illuminant;
+ (instancetype) XYZColorWithX: (double) x Y: (double) y Z: (double)z usingIll: (VTColorUtilsIlluminant) illuminant usingObserver:(VTColorUtilsObserver)observer;
- (VTRGBColor *) RGBColorInSpace: (VTColorUtilsRGBSpace) colorSpace;
- (VTLuvColor *) toLUV;


- (VTXYZColor*) toXYZWithIlluminant: (VTColorUtilsIlluminant) illuminate;
- (VTLchColor*) toLCH;
- (VTLabColor*) toLab;
@end
