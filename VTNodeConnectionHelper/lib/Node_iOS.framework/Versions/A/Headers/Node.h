//
//  Node.h
//  Node
//
//  Created by Wade Gasior on 6/9/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//

#ifndef Node_Node_h
#define Node_Node_h

/*!
 *  @header
 *	@file Node.h
 *
 *  @discussion Node API
 *
 *	@copyright 2012-2015 Variable Technologies. All rights reserved.
 */

#define NODE_API_REVISION           4.3.4
#define NODE_API_REVISION_STRING    @"4.3.4"

#import <CoreBluetooth/CoreBluetooth.h>
#import <Foundation/Foundation.h>
#import "VTNodeTypes.h"
#import "NodeDeviceDelegate.h"
#import "VTSensorReading.h"
#import "VTQuatReading.h"
#import "VTNodeDevice.h"
#import "VTNodeDevice+Chroma.h"
#import "VTNodeDevice+Photon.h"
#import "VIColorReading.h"
#import "VIChromaMatch.h"
#import "VIChromaMatchResult.h"
#import "VTColorUtils.h"
#import "VTRGBColor.h"
#import "VTXYZColor.h"
#import "VTLabColor.h"
#import "VTLCHColor.h"
#import "VTHSBColor.h"
#import "VIChroma.h"
#import "VIAnalytics.h"
#import "VISpectralColor.h"
#import "VIPhotonPixelReading.h"
#import "VTNodeDevice+Therma2.h"
#endif
