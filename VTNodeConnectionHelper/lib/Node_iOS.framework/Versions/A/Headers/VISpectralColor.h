//
//  VTSpectralColor.h
//  Node_iOS
//
//  Created by Corey Mann on 1/14/16.
//  Copyright © 2016 Variable, Inc. All rights reserved.
//

#ifndef VISpectralColor_h
#define VISpectralColor_h
#include "VTColorUtils.h"


@class VIPhotonPixelReading;
@class VTXYZColor;

@interface VISpectralColor : NSObject

    @property (readonly) VIPhotonPixelReading* photonPixels;
    @property (readonly) NSUInteger startingWavelength;
    @property (readonly) NSUInteger endingWavelength;

    @property (readonly, strong) NSData* rawIntensities;

/**
 *  returns the minium wavelengtj that VISpectralColor is capable of using
 */
+ (NSUInteger) minimumWavelength;

/**
 *  returns the maximum wavelength that VISpectralColor is capable of using.
 */
+ (NSUInteger) maximumWavelength;

/**
 *  returns the maximum wavelength that can be detected in this framework. Note: that this value is excluded.
 */
+ (NSUInteger) maximumVariableWavelengthExclude;

/**
 *  return the minimum wavelength that can be detected in this framework. Note: this value is included.
 */
+ (NSUInteger) minimumVariableWavelengthInclude;

/**
 * Initialize a new VISpectralColor with the minimum and maximum variable wavelengths.
 */
-(id) initWithSpectrumValue: (NSData*) intentisities withPixelReading: (VIPhotonPixelReading*) reading;
-(id) initWithSpectrumValue: (NSData*) intentisities fromWavelength: (NSUInteger) startWavelength toWavelength: (NSUInteger) endWavelength  withPixelReading: (VIPhotonPixelReading*) reading;


/**
 *
 * @return a copy of the mSpectrum values trimmed to exact measurement.
 */
- (NSArray*) getTrimmedSpectrum;

- (VTXYZColor*) toXYZ;
- (VTXYZColor*) toXYZWithObserver: (VTColorUtilsObserver) observer withIlluminant: (VTColorUtilsIlluminant) illuminant;

@end

#endif /* VTSpectralColor_h */
